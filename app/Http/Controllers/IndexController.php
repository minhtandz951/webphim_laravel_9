<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Genre;
use App\Models\Country;
use App\Models\Movie;
use App\Models\Episode;
use App\Models\Movie_Genre;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{

    public function timkiem(){
        if(isset($_GET['search'])){
            $search = $_GET['search'];
            $phimhot_sidebar = Movie::where('phim_hot',1)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
            $phimhot_trailler = Movie::where('resolution',5)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
            $category = Category::orderBy('position','ASC')->where('status',1)->get();
            $genre = Genre::orderBy('id','DESC')->get();
            $country = Country::orderBy('id','DESC')->get();
            //$cate_slug = Category::where('title','LIKE', '%'.$search.'%' )->first();
            $movie = Movie::where('title','LIKE', '%'.$search.'%' )->orderBy('ngaycapnhap', 'DESC')->paginate(40);
            return view('pages.timkiem', compact('category','genre','country','movie', 'phimhot_sidebar','phimhot_trailler','search'));
        }
        else{
            return redirect()->to('/');
        }
       
    }
    public function home(){
        $phimhot = Movie::where('phim_hot',1)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->get();
        $phimhot_sidebar = Movie::where('phim_hot',1)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $phimhot_trailler = Movie::where('resolution',5)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $category = Category::orderBy('position','ASC')->where('status',1)->get();
        $genre = Genre::orderBy('id','DESC')->get();
        $country = Country::orderBy('id','DESC')->get();
        $category_home = Category::with('movie')->orderBy('id','DESC')->where('status',1)->get();
    	return view('pages.home', compact('category','genre','country','category_home','phimhot', 'phimhot_sidebar','phimhot_trailler'));
    }
    public function category($slug){
        $phimhot_sidebar = Movie::where('phim_hot',1)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $phimhot_trailler = Movie::where('resolution',5)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $category = Category::orderBy('position','ASC')->where('status',1)->get();
        $genre = Genre::orderBy('id','DESC')->get();
        $country = Country::orderBy('id','DESC')->get();

        $cate_slug = Category::where('slug',$slug)->first();
        $movie = Movie::where('category_id',$cate_slug->id)->orderBy('ngaycapnhap', 'DESC')->paginate(40);
    	return view('pages.category', compact('category','genre','country','cate_slug','movie', 'phimhot_sidebar','phimhot_trailler'));
    }


    public function genre($slug){
        $phimhot_sidebar = Movie::where('phim_hot',1)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $phimhot_trailler = Movie::where('resolution',5)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $category = Category::orderBy('position','ASC')->where('status',1)->get();
        $genre = Genre::orderBy('id','DESC')->get();
        $country = Country::orderBy('id','DESC')->get();
        $genre_slug = Genre::where('slug',$slug)->first();
        //Nhiều thể loại
        $movie_genre = Movie_Genre::where('genre_id', $genre_slug->id)->get();
        $many_genre = [];
        foreach($movie_genre as $key => $movi) {
            $many_genre[] = $movi->movie_id;
        }
        // return response()->json($many_genre);
        $movie = Movie::whereIN('id',$many_genre)->orderBy('ngaycapnhap', 'DESC')->paginate(40);
    	return view('pages.genre', compact('category','genre','country','genre_slug','movie', 'phimhot_sidebar','phimhot_trailler','many_genre'));
    }
    public function country($slug){
        $phimhot_sidebar = Movie::where('phim_hot',1)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $phimhot_trailler = Movie::where('resolution',5)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $category = Category::orderBy('position','ASC')->where('status',1)->get();
        $genre = Genre::orderBy('id','DESC')->get();
        $country = Country::orderBy('id','DESC')->get();

        $country_slug = Country::where('slug',$slug)->first();
        $movie = Movie::where('country_id',$country_slug->id)->orderBy('ngaycapnhap', 'DESC')->paginate(10);
    	return view('pages.country', compact('category','genre','country','country_slug','movie', 'phimhot_sidebar','phimhot_trailler'));
    }

    public function year($year){
        $phimhot_sidebar = Movie::where('phim_hot',1)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $phimhot_trailler = Movie::where('resolution',5)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $category = Category::orderBy('position','ASC')->where('status',1)->get();
        $genre = Genre::orderBy('id','DESC')->get();
        $country = Country::orderBy('id','DESC')->get();

        $year = $year;
        $movie = Movie::where('year',$year)->orderBy('ngaycapnhap', 'DESC')->paginate(40);
    	return view('pages.year', compact('category','genre','country','year','movie', 'phimhot_sidebar','phimhot_trailler'));
    }

    public function tag($tag){
        $phimhot_sidebar = Movie::where('phim_hot',1)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $phimhot_trailler = Movie::where('resolution',5)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $category = Category::orderBy('position','ASC')->where('status',1)->get();
        $genre = Genre::orderBy('id','DESC')->get();
        $country = Country::orderBy('id','DESC')->get();
        $tag = $tag;
        $movie = Movie::where('tags','LIKE', '%'.$tag. '%')->orderBy('ngaycapnhap', 'DESC')->paginate(40);
    	return view('pages.tag', compact('category','genre','country','tag','movie', 'phimhot_sidebar','phimhot_trailler'));
    }

    public function movie($slug){
        $phimhot_sidebar = Movie::where('phim_hot',1)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $phimhot_trailler = Movie::where('resolution',5)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $category = Category::orderBy('position','ASC')->where('status',1)->get();
        $genre = Genre::orderBy('id','DESC')->get();
        $country = Country::orderBy('id','DESC')->get();
        $movie = Movie::with('category','genre','country','movie_genre')->where('slug',$slug)->where('status',1)->first();
        $related = Movie::with('category','genre','country')->where('category_id',$movie->category->id)->orderBy(DB::raw('RAND()'))->whereNotIn('slug',[$slug])->get();
        //lấy tập đầu
        $episode_tapdau = Episode::with('movie')->where('movie_id', $movie->id)->orderBy('episode','ASC')->take(1)->get()->first();
        //Lấy 3 tập mới nhất
        $episode = Episode::with('movie')->where('movie_id',$movie->id)->orderBy('episode','DESC')->take(3)->get();

        //Lấy tập phim đã thêm
        $episode_current_list = Episode::with('movie')->where('movie_id',$movie->id)->get();
        $episode_current_list_count = $episode_current_list->count()
;
    	return view('pages.movie', compact('category','genre','country','movie','related', 'phimhot_sidebar','phimhot_trailler','episode','episode_tapdau','episode_current_list_count'));
    }
    public function watch($slug,$tap){

        $phimhot_sidebar = Movie::where('phim_hot',1)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $phimhot_trailler = Movie::where('resolution',5)->where('status',1)->orderBy('ngaycapnhap', 'DESC')->take(10)->get();
        $category = Category::orderBy('position','ASC')->where('status',1)->get();
        $genre = Genre::orderBy('id','DESC')->get();
        $country = Country::orderBy('id','DESC')->get();
        
        $movie = Movie::with('category','genre','country','movie_genre','episode')->where('slug',$slug)->where('status',1)->first();  
        $related = Movie::with('category','genre','country','movie_genre')->where('category_id',$movie->category->id)->orderBy(DB::raw('RAND()'))->whereNotIn('slug',[$slug])->get();
        //lấy tập 1 tap-FullHD
        if(isset($tap)){
            
            $tap_phim = $tap;
            $tap_phim = substr($tap_phim, 4,20);
            
        }
        else{

            $tap_phim =1;
        }
        $episode = Episode::where('movie_id', $movie->id)->where('episode', $tap_phim)->first();
    	return view('pages.watch', compact('category','genre','country','movie', 'phimhot_sidebar','phimhot_trailler','related','episode','tap_phim'));

    }
    public function episode(){
    	return view('pages.episode');
    }
}
